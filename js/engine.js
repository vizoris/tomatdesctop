$(function() {

	 $('#datetimepicker1').datetimepicker({
	    locale: 'ru',
	    format: 'DD/MM/YYYY'
	  });

	  $('#datetimepicker2').datetimepicker({
	    locale: 'ru',
	    format: 'DD/MM/YYYY'
	  });

	  $('.order-filter__btn').click(function() {
	  	$(this).toggleClass('active');
	  	$(this).next('.order-filter__list').slideToggle();
	  })

	  // Показать сайдбар на планшетах

	  $('.sidebar-btn').click(function() {
	  	$(this).toggleClass('active');
	  	$('.sidebar').toggleClass('active');
	  })
})